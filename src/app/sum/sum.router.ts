import { Router } from "express";

export const router = Router();

router.get("/sum/:a/:b", (req, res) => {
  const a = parseInt(req.params.a);
  const b = parseInt(req.params.b);

  if (isNaN(a) || isNaN(b)) {
    return res.status(200).send({ msg: `Sum: NaN` });
  }
  const sum = a + b;

  res.send({ msg: `Sum: ${sum}` });
});
